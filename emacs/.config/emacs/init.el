;;; init.el --- Emacs configuration                  -*- lexical-binding: t; -*-

;; Author:  Soham Pal
;; Keywords: configuration, lisp

;;; License

;; Everyone is permitted to do whatever they like with this software
;; without limitation.  This software comes without any warranty
;; whatsoever.

;;; Commentary:

;; My init.el, using bits and pieces from others'. Requires at least Emacs 28.1.

;;; Code:

(require 'package)

;; package sources, prefer elpa over melpa
(setq package-archives
      '(("elpa" . "https://elpa.gnu.org/packages/")
        ("nongnu" . "https://elpa.nongnu.org/nongnu/")
        ("melpa" . "https://melpa.org/packages/")))

(setq package-archive-priorities
      '(("elpa" . 2)
        ("nongnu" . 1)))

;; `setup' for concisely installing and configuring packages
(unless (package-installed-p 'setup)
  (unless (memq ',package package-archive-contents)
    (package-refresh-contents))
  (package-install 'setup))
(eval-when-compile
  (package-initialize)
  (require 'setup))

(setup-define :load-after
    (lambda (&rest features)
      (let ((body `(require ',(setup-get 'feature))))
        (dolist (feature (nreverse features))
          (setq body `(with-eval-after-load ',feature ,body)))
        body))
    :documentation "Load the current feature after FEATURES.")

(setup-define :package
  (lambda (package)
    (if (consp package)
        `(unless (and (package-installed-p ',(car package))
                      (package-vc-p (cadr (assoc ',(car package)
                                                 (package--alist)))))
           (package-vc-install
            ,(if (consp (cdr package))
                 `',(car package)
               `(list ',(car package) ,@(cdr package)))))
      `(unless (package-installed-p ',package)
         (unless (memq ',package package-archive-contents)
           (package-refresh-contents))
         (package-install ',package))))
  :documentation "Install PACKAGE if it hasn't been installed yet.
The first PACKAGE can be used to deduce the feature context.  If
PACKAGE is a cons-cell, then the it will be interpreted as a
package specification that will be passed to
`package-vc-install'."
  :repeatable t
  :shorthand (lambda (form)
               (if (consp (cadr form)) (caadr form) (cadr form))))

;; Load local customizations
(setq custom-file (locate-user-emacs-file "custom.el"))
(load custom-file t)

;; Useroptions and packages
;;; Display options
(setopt window-resize-pixelwise t
        frame-resize-pixelwise t
        blink-cursor-mode nil
        use-dialog-box nil
        menu-bar-mode nil
        tool-bar-mode nil
        scroll-bar-mode nil)

;;; Appearance
(setopt global-display-line-numbers-mode 'visual
        column-number-mode t)

(defun site/complement-appearance () ;; copied from pkal's config
  "Apply the complemented colours of the current appearance."
  (interactive)
  (require 'color)
  (dolist (face '(default mode-line tab-bar))
    (let* ((fg (face-attribute face :foreground))
           (fg (if (eq fg 'undefined) "black" fg))
           (fg* (color-complement-hex fg))
           (bg (face-attribute face :background))
           (bg (if (eq bg 'undefined) "white" bg))
           (bg* (color-complement-hex bg)))
      (set-face-attribute face nil :foreground fg*)
      (set-face-attribute face nil :background bg*))))
(add-hook 'after-init-hook #'site/complement-appearance)
(global-set-key (kbd "C-c t") #'site/complement-appearance)

;;; Startup options
(setopt inhibit-startup-screen t
        initial-scratch-message nil
        initial-buffer-choice (lambda ()
                                (org-agenda-list)
                                (get-buffer "*Org Agenda*")
                                (delete-other-windows))
        initial-major-mode 'fundamental-mode
        use-short-answers t)

;;; Backups, autosaves, and lockfiles
(let ((backup-dir (locate-user-emacs-file "backup/"))
			(auto-save-dir (locate-user-emacs-file "auto-save/")))
	(unless (file-directory-p backup-dir)
		(make-directory backup-dir))
	(unless (file-directory-p auto-save-dir)
		(make-directory auto-save-dir))
	(setopt backup-directory-alist `((".*" . ,backup-dir))
          auto-save-file-name-transforms `((".*" ,auto-save-dir t))
          create-lockfiles nil
          backup-by-copying t))

(setup (:package vc-backup) ;; backup with version control
  (setopt version-control t
          kept-old-versions 8
          kept-new-versions 32
          delete-old-versions t))

;;; Kill and yank
(setopt save-interprogram-paste-before-kill t
        kill-do-not-save-duplicates t)
(global-set-key [remap yank] #'yank-from-kill-ring)

;;; Scrolling
(setopt fast-but-imprecise-scrolling t
        pixel-scroll-precision-mode t
        pixel-scroll-mode t)

;;; Buffer names
(setopt uniquify-buffer-name-style 'forward)

;;; History
(setopt history-delete-duplicates t
        savehist-mode t
        save-place-mode t
        recentf-mode t
        recentf-max-saved-items 64)

(defun site/recentf-open-files () ;; adapted from mastering emacs
  "Use `completing-read' to \\[find-file] a recent file."
  (interactive)
  (find-file (completing-read "Find recent file: " recentf-list)))
(global-set-key (kbd "C-x C-r") #'site/recentf-open-files) ;; C-] to quit `find-file'

;;; Help
(setopt help-window-select t)

;;; Authentication
(setopt epg-gpg-program "gpg2"
        epa-pinentry-mode 'loopback
        auth-sources
        '("~/.authinfo.gpg" "~/.authinfo" "~/.netrc"))

;;;File management
;; (setup dired
;;   (:also-load dired-x)
;;   (setopt dired-dwim-target t
;;           dired-isearch-filenames 'dwim
;;           dired-recursive-copies 'always
;;           dired-recursive-deletes 'top
;;           dired-ls-F-marks-symlinks t
;;           dired-auto-revert-buffer t)
;;   (:hook auto-revert-mode))

(setup (:package dirvish)
  (dirvish-override-dired-mode)
  (dirvish-side-follow-mode)
  (setopt dirvish-quick-access-entries
          '(("h" "~/" "Home")
            ("d" "~/Downloads" "Downloads")
            ("D" "~/Documents" "Documents")
            ("p" "~/Projects" "Projects"))
          dirvish-mode-line-format
          '(:left (sort symlink) :right (omit yank index))
          dirvish-attributes
          '(file-time file-size collapse subtree-state vc-state git-msg)
          dired-listing-switches
          "-l --almost-all --human-readable --group-directories-first --no-group"
          dired-auto-revert-buffer t)
  (let ((zathura-exts '("pdf" "djvu" "ps" "cbz" "cbr" "cbt" "cb7"))
        (office-exts '("doc" "docx" "odt" "ods" "xls" "xlsx" "rtf" "odp" "ppt" "pptx")))
    (add-to-list 'dirvish-open-with-programs
                 `(,zathura-exts . ("zathura" "%f")))
    (add-to-list 'dirvish-open-with-programs
                 `(,office-exts . ("libreoffice" "%f"))))
  (:bind-into dirvish-mode-map
    "a" #'dirvish-quick-access
    "i" #'dirvish-file-info-menu
    "y" #'dirvish-yank-menu
    "N" #'dirvish-narrow
    "s" #'dirvish-quicksort
    "v" #'dirvish-vc-menu)
  (:global "C-c d" #'dirvish-fd))

;;; Searching
(setup search-default-mode #'char-fold-to-regexp
       isearch-wrap-pause t
       isearch-lazy-count t
       isearch-lazy-highlight t)

;;; Editing
(setopt electric-pair-mode t
        electric-pair-open-newline-between-pairs t
        show-paren-mode t
        delete-selection-mode t
        enable-recursive-minibuffers t
        indent-tabs-mode nil
        tab-width 2
        sentence-end-double-space nil)

;;; Flymake
(setup flymake
  (:bind "M-n" #'flymake-goto-next-error
         "M-p" #'flymake-goto-prev-error)
  (:hook-into prog-mode LaTeX-mode))

;;;;; Spell checking
(setopt ispell-program-name "aspell"
        ispell-dictionary "english"
        flyspell-issue-welcome-flag nil
        flyspell-issue-message-flag nil
        flyspell-use-meta-tab t
        dictionary-server "dict.org")

(add-hook 'text-mode-hook #'flyspell-mode)
(add-hook 'prog-mode-hook #'flyspell-prog-mode)
(global-set-key (kbd "C-c f") #'dictionary-search)

;;; Emacs in browser
(setup (:package atomic-chrome)
  (atomic-chrome-start-server))

;;; Abbreviations, completions, and co.
(setopt read-buffer-completion-ignore-case t
        completion-ignore-case t
        tab-always-indent 'complete
        completion-auto-select 'second-tab
        completion-auto-wrap t)

(setup minibuffer
  ;; Completions sort functions from https://robbmann.io/posts/emacs-29-completions/
  (defun site/sort-by-alpha-length (elems)
    "Sort ELEMS first alphabetically, then by length."
    (sort elems (lambda (c1 c2)
                  (or (string-version-lessp c1 c2)
                      (< (length c1) (length c2))))))

  (defun site/sort-by-history (elems)
    "Sort ELEMS by minibuffer history.
Use `site/sort-sort-by-alpha-length' if no history is available."
    (if-let ((hist (and (not (eq minibuffer-history-variable t))
                        (symbol-value minibuffer-history-variable))))
        (minibuffer--sort-by-position hist elems)
    (site/sort-by-alpha-length elems)))

  (defun site/completion-category ()
    "Return completion category."
    (when-let ((window (active-minibuffer-window)))
      (with-current-buffer (window-buffer window)
        (completion-metadata-get
         (completion-metadata (buffer-substring-no-properties
                               (minibuffer-prompt-end)
                               (max (minibuffer-prompt-end) (point)))
                              minibuffer-completion-table
                              minibuffer-completion-predicate)
         'category))))

  (defun site/sort-multi-category (elems)
    "Sort ELEMS per completion category."
    (pcase (site/completion-category)
      ('nil elems) ; no sorting
      ('kill-ring elems)
      ('project-file (site/sort-by-alpha-length elems))
      (_ (site/sort-by-history elems))))

  (setopt read-file-name-completion-ignore-case t
          completions-format 'one-column
          completions-header-format nil
          completions-max-height 20
          completion-auto-help 'always
          completions-sort #'site/sort-multi-category)
  
  (:bind-into minibuffer-mode-map
    "C-n" #'minibuffer-next-completion
    "C-p" #'minibuffer-previous-completion)
  (:bind-into completion-in-region-mode-map
    "C-n" #'minibuffer-next-completion
    "C-p" #'minibuffer-previous-completion))

;; (setup (:package vertico)
;;   (:also-load vertico-directory)
;;   (setopt vertico-cycle t
;;           vertico-mode t)
;;   (:bind-into vertico-map
;;     "RET" #'vertico-directory-enter
;;     "DEL" #'vertico-directory-delete-char
;;     "M-DEL" #'vertico-directory-delete-word
;;     "M-RET" #'vertico-exit-input)
;;   (:global "C-c v" #'vertico-flat-mode))

(setup (:package marginalia)
  (setopt marginalia-mode t)
  (:bind-into minibuffer-local-map
    "M-A" #'marginalia-cycle))

(setup (:package orderless)
  (setopt completion-styles '(orderless emacs22)))

;; (setup (:package corfu)
;;   (:also-load corfu-history corfu-info) ; extensions
;;   (setopt corfu-history-mode t
;; 					corfu-cycle t)
;;   (add-to-list 'savehist-additional-variables 'corfu-history)
;;   (:bind-into corfu-map
;;     "C-d" corfu-info-documentation
;;     "C-s" corfu-info-location)
;;   (global-corfu-mode))

;; (setup (:package (corfu-terminal
;;                   :url "https://codeberg.org/akib/emacs-corfu-terminal.git"))
;;   (unless (display-graphic-p)
;;     (corfu-terminal-mode)))

(setup (:package cape)
	(dolist (cape-fn '(cape-file cape-keyword cape-tex cape-ispell cape-dabbrev))
		(add-to-list 'completion-at-point-functions cape-fn :append)))

(setup (:package tempel tempel-collection)
  (defun site/tempel-setup-capf ()
    ;; Add the Tempel Capf to `completion-at-point-functions'.
    ;; `tempel-expand' only triggers on exact matches. Alternatively use
    ;; `tempel-complete' if you want to see all matches, but then you
    ;; should also configure `tempel-trigger-prefix', such that Tempel
    ;; does not trigger too often when you don't expect it. NOTE: We add
    ;; `tempel-expand' *before* the main programming mode Capf, such
    ;; that it will be tried first.
    (setq-local completion-at-point-functions
                (cons #'tempel-expand completion-at-point-functions)))
  (add-hook 'prog-mode-hook #'site/tempel-setup-capf)
  (add-hook 'text-mode-hook #'site/tempel-setup-capf)
  (:global "M-+" #'tempel-complete
           "M-*" #'tempel-insert))

;;; Key completions
(setup (:package which-key)
  (:hook-into after-init))

;;; Project management
(setup project
  (setopt project-switch-use-entire-map t))

;;; Cross referencing
(setopt xref-show-definitions-function #'xref-show-definitions-buffer-at-bottom
        xref-auto-jump-to-first-definition 'show
        xref-search-program (if (executable-find "rg") 'ripgrep 'grep))

;;; Tree-sitter
(when (treesit-available-p)
  (add-to-list 'major-mode-remap-alist '(python-mode . python-ts-mode)))

;;; Shell
(setup (:package exec-path-from-shell))

;;; Direnv
(setup (:and (executable-find "direnv") envrc-global-mode)
  (:package envrc)
      (:bind-into envrc-mode-map
        "C-c e" #'envrc-command-map)
      (:hook-into after-init))

;;; Code folding
(setup outline-minor-mode
  (:hook-into prog-mode text-mode))

(setup hs-minor-mode
  (:hook-into prog-mode))

(setup (:package bicycle)
  (:load-after outline)
  (:bind-into outline-minor-mode-map
    "C-<tab>" #'bicycle-cycle
    "S-<tab>" #'bicycle-cycle-global))

;;; Dynamic expansions
(setopt hippie-expand-try-functions-list
        '(try-expand-all-abbrevs
          try-expand-dabbrev-visible
          try-complete-file-name-partially
          try-expand-dabbrev
          try-complete-file-name
          try-expand-dabbrev-all-buffers
          try-expand-dabbrev-from-kill))

(global-set-key (kbd "M-/") #'hippie-expand)

;;; Version control
(setup (:package diff-hl)
  (setopt diff-hl-flydiff-mode t
          diff-hl-margin-mode t)
  (:hook-into prog-mode))

;;; Reformat code
(setup (:package reformatter))

;;; LaTeX
(setup (:and (executable-find "latex") LaTeX)
  (:package auctex)
  (setopt TeX-auto-save t
          TeX-parse-self t
          TeX-master 'dwim
          TeX-electric-math '("\\(" . "\\)")
          TeX-electric-sub-and-superscript t
          LaTeX-electric-left-right-brace t
          bibtex-maintain-sorted-entries t
          reftex-plug-into-AUCTeX t
          reftex-enable-partial-scans t)
  (:hook visual-line-mode TeX-fold-mode LaTeX-math-mode reftex-mode))

;;; Markdown
(setup (:package markdown-mode)
  (:hook visual-line-mode))

;;; Org mode
(setup org-mode (:also-load ox-md)
  (setopt org-highlight-latex-and-related '(native latex script entities)
          org-preview-latex-image-directory "/tmp/ltxpng"
          org-use-speed-commands t
          org-directory "~/Org")

  ;; Getting things done with Org
  ;; Adapted from https://emacs.cafe/emacs/orgmode/gtd/2017/06/30/orgmode-gtd.html
  (let ((gtd-dir (expand-file-name "gtd" org-directory)))
    (unless (file-directory-p gtd-dir)
      (mkdir gtd-dir t))
    (setopt org-agenda-files (list gtd-dir))
    (setopt org-capture-templates
            `(("t" "Todo [inbox]" entry
               (file+headline ,(expand-file-name "inbox.org" gtd-dir) "Tasks")
               "* TODO %i%?")
              ("T" "Tickler" entry
               (file+headline ,(expand-file-name "tickler.org" gtd-dir) "Tickler")
               "* %i%? \n %^T")))
    (setopt org-refile-targets
            `((,(expand-file-name "projects.org" gtd-dir) :maxlevel . 3)
              (,(expand-file-name "backlog.org" gtd-dir) :level . 1)
              (,(expand-file-name "tickler.org" gtd-dir) :maxlevel . 2)))
    (setopt org-todo-keywords
            '((sequence "TODO(t)" "WAITING(w)" "|" "DONE(d)" "CANCELLED(c)")))
    (setopt org-agenda-custom-commands
            '(("w" "Work" tags-todo "@work"
               ((org-agenda-overriding-header "Work")
                ;; (org-agenda-skip-function #'site/org-agenda-skip-all-siblings-but-first)
                ))
              ("h" "Home" tags-todo "@home"
               ((org-agenda-overriding-header "Home")
                ;; (org-agenda-skip-function #'site/org-agenda-skip-all-siblings-but-first)
                )))))

  (defun site/org-agenda-skip-all-siblings-but-first ()
    "Skip all but the first non-done entry."
    (let (should-skip-entry)
      (unless (site/org-current-is-todo)
        (setq should-skip-entry t))
      (save-excursion
        (while (and (not should-skip-entry) (org-goto-sibling t))
          (when (site/org-current-is-todo)
            (setq should-skip-entry t))))
      (when should-skip-entry
        (or (outline-next-heading)
            (goto-char (point-max))))))

  (defun site/org-current-is-todo ()
    "Check if the current entry is a TODO."
    (string= "TODO" (org-get-todo-state)))

  (:global "C-c l" #'org-store-link
           "C-c a" #'org-agenda
           "C-c c" #'org-capture
           "C-c b" #'org-switchb)
  
  (:hook visual-line-mode))

;;; LSP client
(setup eglot
  (setopt eglot-autoshutdown t
          eglot-extend-to-xref t
          eldoc-idle-delay 0.1)
  (:bind "C-c a" #'eglot-code-actions
         "C-c z" #'eglot-format
         "C-c r" #'eglot-rename))

;;; Python
(setup python
  (when (executable-find "ipython3") ;; needs IPython to be installed globally
    (setq python-shell-interpreter "ipython3"
          python-shell-interpreter-args "--simple-prompt")))

(setup (:if-package reformatter)
  (reformatter-define isort-format :program "isort" :args '("--stdout" "--atomic" "--quiet" "-"))
  (reformatter-define black-format :program "black" :args '("-")))

;; Utility functions
(defun site/untabify-buffer ()
  "Convert all tabs in current buffer to spaces."
  (interactive)
  (untabify (point-min) (point-max)))

(defun site/maybe-download-file (fname dir url)
  "If FNAME is not present in DIR then download it from URL."
  (require 'url)
  (when (not (file-exists-p (concat (file-name-as-directory dir) fname)))
    (when (not (file-directory-p dir))
      (make-directory dir))
    (url-copy-file url (concat (file-name-as-directory dir) fname))))

(defun site/map-cartesian-product (function list1 list2)
  "Map FUNCTION over Cartesian product of LIST1 and LIST2."
  (require 'cl-lib)
  (cl-loop for elem1 in list1
           collect (cl-loop for elem2 in list2
                        collect (funcall function (list elem1 elem2)))))


(provide 'init)
;;; init.el ends here
